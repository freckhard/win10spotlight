# Motivation
I have seen a nice desktop wallpaper which turned out to be a Windows 10 Spotlight image. Microsoft provides this service releasing two new wallpapers (in protrait and landscape orientation) almost every day. By looking for a collection of those I came upon this non-official webpage https://windows10spotlight.com/ where someone build a wordpress pushing those images to regularly. The maintainer of that page states https://windows10spotlight.com/about that they do not intend to provide those images in an archive and they do not want to be asked to offer that service. Well I do offer this to you know! Because I wanted to see all the wallpapers not just on this wordpress page only 5 at a time but rather as files on my harddrive I hacked down this python script that does the job in downloading all the image files! I would appreciate your help to make the code read & look more nicely, cleaned up and tidy, so pull requests are more than very welcome (I would love to learn from you)! As I am quite frustrated by now in dealing with all those edge cases, bugs and non-continuities that come to shine every few weeks, I am lacking the motiviation to refactor this to a beauty myself. Anyway, you can read more about my troubles below.

# Requirements
Have pip install the following python packages (into your virtual environment) and you are good to go:
- requests
- pyexif
- argparse
That's it.

# Usage
usage: win10spotlight_downloader.py [-h] [--seenlimit SEENLIMIT] [--startpage STARTPAGE]
               [--lastpage LASTPAGE] [--downloads {full,1,0}]
               [--justlinks {1,0}] [--dest DEST]

optional arguments:

--seenlimit int             Stop after this many duplicates    Default: 10 (two pages)
                            (or 0 for a full check through the last page)
--startpage int             Start looking from this page on    Default: Mainpage
--lastpage  int             Stop after looking at this page    Default: all
--downloads {full,1,0}      Change the download behavior       Default: yes
       full   Do a complete redownload of all images
       1      Download new images
       0      No images will be downloaded
--justlinks {1,0}           Only rebuild the links list, like downloads 0
--dest                      Change the target download directory

## Observations
New spotlight images are added to the front and top of the wordpress main page. This almost happens daily but on sundays and most of the time two new motives are added. By doing so all existing pictures are pushed down, constantly increasing the last pages page number. All pages do contain 5 picture pages but the very last, which gets the rest of the rolled over ones and therefor commonly contains less. On a regular picture page there are two pictures of the same motif, in landscape and portrait mode respectively. The UUID of the image page is identical to the UUID of the landscape file name, meaning ...images/abcdefgh corresponds to landscape abcdefgh.jpg. Most of the pictures are of the JPEG format, but some older pages do contain PNG images.

# General problems:
 - 1. The mainpage `.com`, `page/0/` and `page/1/` do display most of the time the same 5 photos. But this may not always be the case, meaning those pages could differ in one or more images. To tackle this problem we need to always analyse those three pages. The reason for that behaviour is unknown to me, but can be mitigated by a sufficient large enough `--seenlimit`, so that those pages always get checked entirely.

 - 2. By the images rolling over to the next page when new ones are added, it has occured that those image pages leaving page X out of the bottom would sometimes not appear on top of the next page X+1. For the time being they are lost and can't be found until the next time/day a new set of windows 10 spotlight images are added, causing another rollover into the next page making them appear again (and sometimes hiding others). For the initial download of all images I recommend to rerun this script with --seenlimit 0 the next to days.

 - 3. Sometimes an UUID of the image page is not identical to the image UUID of the picture in landsacpe mode. The script will notify in such a case and handle the download properly. But it can occur, that the found and printed UUID may then of course not be the found on the manually visited page/Y/, so don't get confused.

# Afterthoughts:
I have a fetish for equal lengthy variable names, hence "hoch" and "quer" for landscape and portrait (in German), which unfortunately are not of the same length in English. I tried to find clever abbrevations or matching synonyms but to no avail. At this point I have given up, so if you like "portrait" and "landscape" better, feel free to change it or even find a better naming scheme that soothes my OCD.

# Attention
 - The script sometimes needs to be rerun, for whatever reasons, better error handling is needed.
 - The script has not been tested on windows, but should work


## ToDos
 - parallel / async downloads and exif removal
 - *** Use Pillow um den EXIF Bloat zu entfernen! ***


## Wishlist
 - if processing of an image somehow fails, it should silently retry it up to (maybe) 3 times.
 - Statistics: How many downloaded, how many present, how many insgesamt, how many
        already existing, how many duplicates found after download started, 
 - command line arguments: choose format, hoch, quer, or both.
 - very verbosely output
 - have better error handling: If somehow the program crashes / gets KeyBoardInterrupt, the collected links were lost.
        To circumvent / mitigate that fact, an ugly but standard try catch finally is being used.

