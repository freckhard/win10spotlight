import os
import re
import venv_handler
import requests
import pyexif
import argparse

################################################################################

def main():

# ------------------------------------------------------------------------------

    parser = argparse.ArgumentParser(description='Win10spotlight images downloader.')

    parser.add_argument('--seenlimit', help='Stop after this many duplicates', default=SEEN_LIMIT)
    parser.add_argument('--startpage', help='Start looking from this page on', default=START_PAGE)
    parser.add_argument('--lastpage',  help='Stop after looking at this page', default=LAST_PAGE)
    parser.add_argument('--downloads', help='Change the download behavior', choices=['full', '1', '0'], default='1')
    parser.add_argument('--justlinks', help='Only rebuild the links list, no downloads', choices=['1', '0'], default='0')
    parser.add_argument('--dest', help="Change the target download directory", default='None')

    args = parser.parse_args()

    seen_limit = int(args.seenlimit)
    start_page = int(args.startpage)
    last_page  = int(args.lastpage)
    just_links = int(args.justlinks)

    if args.downloads == "full" and just_links:
        exit("Contradictory arguments: '--downloads full' and '--justlinks 1'")

# ------------------------------------------------------------------------------

    if os.path.exists(args.dest):
        global dir_querformat, dir_hochformat
        dir_querformat = args.dest + '/' + dir_querformat
        dir_hochformat = args.dest + '/' + dir_hochformat

        global links_querformat, links_hochformat
        links_querformat = args.dest + '/' + links_querformat
        links_hochformat = args.dest + '/' + links_hochformat

    # Setup Folders & Files.
    if not just_links:
        create_if_not_existing_folders(dir_querformat)
        create_if_not_existing_folders(dir_hochformat)

    create_if_not_existing_textfiles(links_querformat)
    create_if_not_existing_textfiles(links_hochformat)


    # Create and get list of all filenames present on system.
    file_list = []
    quer_list = []
    hoch_list = []

    if args.downloads == "full":
        downloads = True
        seen_limit = 0
    elif just_links:
        downloads = False
    else:
        file_list = [name.split('.')[0] for _, _, files in os.walk(dir_querformat) for name in files]
        downloads  = int(args.downloads)

    # Loop setup.
    page_number = start_page
    seen_before = 0
    err404 = False
    endreached = False
    limit_reached = False

    try:
        # Loop begin.
        while True:
            
            if page_number < 0:
                page_number = page_number + 1

            if page_number == -1:
                html_page_content = requests.get(url_mainpage).content.decode("utf-8")
                print(f"\n---- {url_mainpage} ----")
            elif page_number > last_page:
                print(f"Desired last page {page_number - 1} finished. Stopping.")
                endreached = True
                break
            else:
                html_page_content = requests.get(url_webpage + str(page_number)).content.decode("utf-8")
                print(f"\n---- page {str(page_number).rjust(4)} ----")

            # Check for a 404 status code, which is most likey when the end is reached.
            if "Oops! That page" in html_page_content:
                print("404 page error reached on page:", page_number)
                err404 = True
                break

            # Find all images on the curent page.
            image_file_links = re.findall(re_image_file_link, html_page_content)

            # Iterate over all found images.
            for image_link in image_file_links:
                
                image = Image_Object(image_link)
                quer_list.append(image.time + '\t' + image.quer_url)
                hoch_list.append(image.time + '\t' + image.hoch_url)

                if image.name in file_list or image.hash in file_list:
                    
                    if image.name in file_list:
                        name = image.name
                    
                    if image.hash in file_list:
                        name = image.hash

                    if image.name != image.hash:
                        name = image.name + "---> " + image.hash
                    
                    print('\033[90m' + name, "exists ✓" + '\033[0m')
                    seen_before = seen_before + 1

                    if seen_limit:
                        if last_page == LAST_PAGE:
                            if seen_limit <= seen_before:
                                limit_reached = True
                                break
                        
                    continue

                else:
                    print(image.name, end=' ')

                    if image.name != image.hash:
                        print("warning! links to --->", image.hash, end=' ')
                        file_list.append(image.hash)
                    
                    if not downloads:
                        print("*is new*")
                    else:
                        print("is new ! Downloading...")

                        file_list.append(image.name)

                        seen_before = 0

                        download(image.quer_url, image.quer_path)
                        download(image.hoch_url, image.hoch_path)
                        image.clean_exif_tags()

            page_number = page_number + 1

            if limit_reached:
                break

    except:
        print("There has been an error!")

    finally:
        # Loop end.
        #  When loop finishes or terminates, print some information.
        if not err404 and not endreached:
            print(f"\nThe last {seen_before} checked pictures have already been downloaded, so we're done! ✓")
        print(f"Last page checked: {url_webpage}{page_number - 1}")

        # get unique elements, but preserve order
        quer_list = list(dict.fromkeys(quer_list))
        hoch_list = list(dict.fromkeys(hoch_list))

        with open(links_querformat, 'r') as file_quer:
            for line in [line.rstrip() for line in file_quer]:
                if line not in quer_list:
                    quer_list.append(line)

        with open(links_hochformat, 'r') as file_hoch:
            for line in [line.rstrip() for line in file_hoch]:
                if line not in hoch_list:
                    hoch_list.append(line)

        quer_list.sort(reverse=True)
        hoch_list.sort(reverse=True)
        
        with open(links_querformat, 'w') as file_quer:
            file_quer.write('\n'.join(quer_list))
        
        with open(links_hochformat, 'w') as file_hoch:
            file_hoch.write('\n'.join(hoch_list))


################################################################################

class Image_Object:

    def __init__(self, image_link):
        self.html  = self.get_html_image_page_content(image_link)
        self.name  = re.search(re_image_file_hash, image_link).group(1)

        self.title = re.search(re_image_titletext, self.html).group(1)
        self.tags  = re.search(re_image_keywords,  self.html).group(1)
        self.date  = re.search(re_image_pubdate,   self.html).group(1)
        self.time  = re.search(re_image_pubdtime,  self.html).group(1)

        self.img_urls = list(dict.fromkeys(re.findall(re_image_serverurl, self.html)))
        self.quer_url = self.img_urls[0]
        self.hoch_url = self.img_urls[1]

        self.filenames = list(dict.fromkeys(re.findall(re_image_file_name, self.html)))
        self.quer_name = self.filenames[0]
        self.hoch_name = self.filenames[1]

        self.quer_path = dir_querformat + '/' + self.date[:-3]
        self.hoch_path = dir_hochformat + '/' + self.date[:-3]

        self.quer_loc = os.path.abspath(self.quer_path) + '/' + self.quer_name
        self.hoch_loc = os.path.abspath(self.hoch_path) + '/' + self.hoch_name

        self.hash  = re.search(re_image_file_hash, self.quer_url).group(1)


# ------------------------------------------------------------------------------

    def get_html_image_page_content(self, image_link):
        html_content = requests.get(image_link).content.decode("utf-8")
        html_content = html_content.split("html-after-content").pop(0)
        return html_content


# ------------------------------------------------------------------------------

    def clean_exif_tags(self):
        self.set_exif_tags(self.quer_loc)
        self.set_exif_tags(self.hoch_loc)


# ------------------------------------------------------------------------------

    def set_exif_tags(self, file):
            exif = pyexif.ExifEditor(file)

            # Save existing modification information
            moddate = exif.getTag('FileModifyDate')
            modtime = moddate.split(' ').pop()
            moddate = self.date.replace('-',':') + ' ' + modtime

            # Remove all exif tags (mostly to get rid of Photoshops' DocumentAncestors)
            os.system(f'exiftool -q -overwrite_original -all= {file} > /dev/null 2>&1')

            # Write exif tags
            exif.setTag('Title', self.title)
            exif.setTag('Subject', self.tags)
            exif.setTag('FileModifyDate', moddate)


################################################################################

def create_if_not_existing_folders(path):
    if not os.path.exists(path):
        os.mkdir(path)


################################################################################

def create_if_not_existing_textfiles(file):
    
    if not os.path.isfile(file):
        open(file, 'w').close()
    

################################################################################

def download(image, path):
    os.system(f'wget -q -N -U firefox {image} -P {path}')


################################################################################

if __name__ == "__main__":

    # Wordpress Links.
    url_mainpage = "https://windows10spotlight.com/"
    url_webpage  = "https://windows10spotlight.com/page/"
    url_imgpage  = "https://windows10spotlight.com/images/"

    # Folders to save images into.
    dir_querformat = "win10spotlight_querformat"
    dir_hochformat = "win10spotlight_hochformat"

    # Textfilenames for saving image file links.
    links_querformat = "win10spotlight_querformat.txt"
    links_hochformat = "win10spotlight_hochformat.txt"

    # regular expressions.
    re_image_file_link = re.compile(r"<h2><a href=\"(.*?)\"")
    re_image_file_name = re.compile(r"(\w{32}\.[jpgn]{3})")
    re_image_file_hash = re.compile(r"(\w{32})")
    re_image_titletext = re.compile(r"title\=\"(.*?)\"")
    re_image_keywords  = re.compile(r"meta name\=\"keywords\"\s+content\=\"(.*?)\"")
    re_image_serverurl = re.compile(r"https://windows10spotlight\.com/wp-content/uploads/\d{4}/\d{2}/\w{32}\.[jpgn]{3}")
    re_image_pubdate   = re.compile(r"datePublished\"\:\"(.*?)T")
    re_image_pubdtime  = re.compile(r"datePublished\":\"(.*?)\+")

    # Defaults.
    SEEN_LIMIT = 25
    START_PAGE = -2
    LAST_PAGE = 10000

    main()
